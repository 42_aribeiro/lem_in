/* ************************************************************************* */
				DESCRIPTION
/* ************************************************************************* */

Auteur : Alexandra Ribeiro
Date de réalisation : 03/2016

Projet :
Votre colonie de fourmis doit se déplacer d'un point à un autre. Mais comment 
faire pour que cela prenne le moins de temps possible ? 
Ce projet utilise les algorithmes de parcours de graphe (DIJKSTRA) : 
ce programme sélectionne intelligemment les chemins et les mouvements précis 
qui doivent être empruntés par ces fourmis.

Ma librairie est composée de : mon printf (ft_printf) + ma libc (lift) + mon get_next_line

Makefile : création de l'executable lem-in
help : -h